##############################################################################
# find Clusters in scope of Unite Plateau 1
# input: csv file with customer-agreements links (one per row)
# output-1: csv file with cluster-customer links (one per row)
# output-2: csv file with cluster-agreement links (one per row)
# Ruud Kapteijn 13-Sep-18

import pandas as pd
from datetime import datetime

cusDict = {}
agrDict = {}

# start of program
print("Start findClusters at %s" % (str(datetime.now())))

# read csv sheet with customer-agreement links
pal = pd.read_csv('PlanBee_20180910.csv')
print("%d customer-agreement links read from csv-file" % (len(pal.index)))

clusterCnt = 1
# for all customer-agreement links in the csv file
for index, row in pal.iterrows():
    # check is customer is in customer dictionary
    if row['N_PERS'] in cusDict:
        # add agreement to agreement dictionary with cluster number of customer
        agrDict[row['PK_CONTRACT_ID']] = cusDict[row['N_PERS']]
    else:
        # check is agreement is in agreement dictionary
        if row['PK_CONTRACT_ID'] in agrDict:
            # add customer to customer dictionary with cluster number of agreement
            cusDict[row['N_PERS']] = agrDict[row['PK_CONTRACT_ID']]
        else:
            # customer and agreement both unkown, create new cluster
            cusDict[row['N_PERS']] = clusterCnt
            agrDict[row['PK_CONTRACT_ID']] = clusterCnt
            clusterCnt += 1

            if clusterCnt % 100000 == 0:
                print '.',

print(' ')
print("%d clusters identified" % (clusterCnt - 1))

# put Dict data in pd.DataFrames and write pd.DataFrames to csv files
pd.DataFrame(cusDict.items()).to_csv("cus.csv", header=["N_PERS", "CLUSTER"], index=False)
pd.DataFrame(agrDict.items()).to_csv("agr.csv", header=["PK_CONTRACT_ID", "CLUSTER"], index=False)

print("End of findClusters at %s" % (str(datetime.now())))
